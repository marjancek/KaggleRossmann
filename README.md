# README #

marjancek's attempts to [Kaggle's](https://www.kaggle.com) "[Rossmann Store Sales](https://www.kaggle.com/c/rossmann-store-sales)"


This is a set of source code files aimed to study and solve a Kaggle's challenge.
Most it is R code.

Feel free to take a look, learn from my mistakes, and leave any constructive comments you may have.